from gui import aken
from kalkulaator import kalkulaator
import sys


try:
    import win32api
    win32api.SetDllDirectory(sys._MEIPASS)
except:
    pass

while True:  # Event Loop
    event, values = aken.Read()
    print(event, values)
    if event is None or event == 'Exit':
        break
    if event == 'Arvuta' and values['esimene'] != '' and values['teine'] != '':
        try:
            aken.FindElement('_summa').Update(kalkulaator(values))
        except:
            aken.FindElement('_summa').Update('Vigane sisend!')
    else:
        aken.FindElement('_summa').Update('Vigane sisend!')

aken.Close()