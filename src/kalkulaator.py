# {'esimene': '', 'teine': '', 0: False, 1: False, 2: False, 3: False}

def kalkulaator(vaartused):
    vastus = 0.0
    try:
        esimene = float(vaartused['esimene'])
    except:
        print("Esimene ei ole number. Kui tahad kasutada komaga arvu kasuta punkti (.)")

    try:
        teine = float(vaartused['teine'])
    except:
        print("Teine ei ole number. Kui tahad kasutada komaga arvu kasuta punkti (.)")

    if vaartused[0]:
        vastus = esimene + teine
    elif vaartused[1]:
        vastus = esimene - teine
    elif vaartused[2]:
        vastus = esimene * teine
    elif vaartused[3]:
        vastus = esimene / teine
    else:
        print("Sellist tehet meil ei ole. Mine otsi mujalt!")
        return None

    return vastus
