import PySimpleGUI as sg

sg.ChangeLookAndFeel('Dark')

image = '../images/kalkulaator.png'
icon = '../images/ikoon.ico'

lahendus = [[sg.Image(filename=image, size=(128, 128), pad=(45, 0))],
          [sg.Txt('0', key='_summa', font=('Rubik-Bold', 15), size=(20, 2), justification='center', text_color='#fafafa'), ],
          [sg.Txt('Sisesta info:', font=('Rubik-Bold', 15), text_color='#fafafa')],
          [sg.Txt('Esimene number', size=(15, 1), font=('Karla', 10), text_color='#aea8d3'), sg.In(size=(8, 1), key='esimene')],
          [sg.Txt('Teine number', size=(15, 1), font=('Karla', 10), text_color='#aea8d3'), sg.In(size=(8, 1), key='teine')],
          [sg.Txt('Tehted:', font=('Rubik-Bold', 15), text_color='#fafafa')],
          [sg.Radio('+', 'tehted', font=('Rubik-Black', 15), text_color='#aea8d3'),
           sg.Radio('-', 'tehted', font=('Rubik-Black', 15), text_color='#aea8d3'),
           sg.Radio('*', 'tehted', font=('Rubik-Black', 15), text_color='#aea8d3'),
           sg.Radio('/', 'tehted', font=('Rubik-Black', 15), text_color='#aea8d3')],
          [sg.Txt(' '*15)],
          [sg.Button('Arvuta', bind_return_key=True, font=('Rubik-Black', 13), button_color=('#fafafa', '#1771FF'), border_width=0, size=(17, 1), pad=(33, 0))],
          [sg.Txt(' '*15)]]

aken = sg.Window('Kalkulaator').Layout(lahendus)
aken.SetIcon(icon=icon)

